
## Visual Studio extension for CafeOBJ language. 

##### Please visit https://github.com/minhcanh99/cafeobj-vscode-extension to install a better version, this repo was no longer maintained (I forgot the account of VSCode extension manager, so we created a new repo as well as new version of this extension)

<s>Site: https://gitlab.com/duongtd23/cafeobj-vscode-extentsion</s>

If you want to improve this extension, please make a pull request. I appreciate your contribution in order to make it more complete!